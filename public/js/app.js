$(function() {
  $('#tabla-perritos').DataTable();
});

$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

function obtenerPerritos(){
$.get('perritos', function(respuesta){
  cargarPerritos(respuesta);
  console.log(respuesta);
});
};

$("#btn-agregar").on('click',function(){
  console.log("asd");
  let nombre = $('#nombre').val();
  let color = $('#color').val();
  let raza = $('#raza').val();
  $.post('perrito', {'nombre': nombre, 'color': color, 'raza': raza}, function() {
  }).done(function(response) {
    var mensaje = $('#mensaje-modal .modal-body .alert');
    mensaje.removeClass("alert-danger").addClass("alert-primary");
    mensaje.text(response);
    $('#crear-perrito').modal("hide");
    $('#mensaje-modal').modal("show");
    $('#nombre').val("");
    $('#color').val("");
    $('#raza').val("");
    obtenerPerritos();
  }).fail(function(response){
    var mensaje = $('#mensaje-modal .modal-body .alert');
    mensaje.text("");
    mensaje.removeClass("alert-primary").addClass("alert-danger");
    response = $.parseJSON(response.responseText);
    $.each(response.errors, function(key, value){
      mensaje.append(value + "<br>");
    });
    $('#mensaje-modal').modal("show");
  });
});

$('#btn-editar').on('click', function(){
  let id = $('#edit-idperrito').val();
  let nombre = $('#edit-nombre').val();
  let color = $('#edit-color').val();
  let raza = $('#edit-raza').val();
  $.ajax({
    url: "perrito/" + id,
    type: 'put',
    data: {"id":id, "nombre":nombre, "color":color, "raza":raza},
  }).done(function(response){
    $('#modificar-perrito').modal("hide");
    var mensaje = $('#mensaje-modal .modal-body .alert');
    mensaje.removeClass("alert-danger").addClass("alert-primary");
    mensaje.text(response);
    $('#mensaje-modal').modal("show");
    setTimeout(function(){
      $(location).attr('href', '/');
    },2000);
    obtenerPerritos();
  }).fail(function(response){
    var mensaje = $('#mensaje-modal .modal-body .alert');
    mensaje.text("");
    mensaje.removeClass("alert-primary").addClass("alert-danger");
    response = jQuery.parseJSON(response.responseText);
    $.each(response.errors, function(key, value){
      mensaje.append(value + "<br>");
    });
    $('#mensaje-modal').modal("show");
  });
  $('#modificar-perrito').modal("hide");
});

function cargarPerritos(perritos){
//limpiar el cuerpo de la tabla
let cuerpo = document.querySelector("#tabla-perritos > tbody");
cuerpo.innerHTML = '';
//Recorro la cantidad de perritos registrados y los agrego a la tabla con sus respectivaos botones e id's.
for(let i=0; i < perritos.length; ++i){
  let tr = $('<tr>');
  let tdAcciones = $('<td>');
  let perrito = perritos[i];
  $('<a>', {
    'class' : 'btn btn-secondary btn-sm btn-editar',
    'data-toggle' : 'modal',
    'data-target' : '#modificar-perrito',
    'href' : 'perrito/' + perrito.id + '/edit',
    'data-id' : perrito.id,
    'data-nombre' : perrito.nombre,
    'data-color' : perrito.color,
    'data-raza' : perrito.raza,
  }).append('<i class="fas fa-cog"></i>').on('click', function(){ 
    $('#edit-idperrito').val($(this).data('id'));
    $('#edit-nombre').val($(this).data('nombre'));
    $('#edit-color').val($(this).data('color'));
    $('#edit-raza').val($(this).data('raza'));
  }).appendTo(tdAcciones);
  $('<a>', {
    'class' : 'btn btn-danger btn-eliminar btn-sm',
    'style' : 'margin-left: 8px',
    'data-toggle' : 'modal',
    'data-target' : '#modal-eliminar',
    'data-id' : perrito.id,
    'data-nombre' : perrito.nombre,
    'data-color' : perrito.color,
    'data-raza' : perrito.raza,
  }).append('<i class="fas fa-trash"></i>').on('click', function(){
    $('#delete-idperrito').val($(this).data('id'))
  }).appendTo(tdAcciones);
  $('<td>').text([i+1]).appendTo(tr);
  $('<td>').text(perrito.nombre).appendTo(tr);
  $('<td>').text(perrito.color).appendTo(tr);
  $('<td>').text(perrito.raza).appendTo(tr);
  tdAcciones.appendTo(tr);
  tr.appendTo(cuerpo);
}
}
$('.btn-editar').on('click', function(){
  $('#edit-idperrito').val($(this).data('id'));
  $('#edit-nombre').val($(this).data('nombre'));
  $('#edit-color').val($(this).data('color'));
  $('#edit-raza').val($(this).data('raza'));
});

$('.btn-eliminar').on('click', function(){
  $('#delete-idperrito').val($(this).data('id'));
});
$('#confirmar').on('click', function(){
  let id = $('#delete-idperrito').val();
eliminarPerrito(id);
});
function eliminarPerrito(id){
$.ajax(
  {
    url: "perrito/" + id,
    type: 'delete',
    data: {"id":id},
  }
).done(function(response){
  obtenerPerritos();
  });
}