<div class="modal-header">
    <h4 class="modal-title">Modificar Perrito</h4>
    <button type="button" class="close" data-dismiss="modal" >
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="container">
        <h3>Modificar información del perrito</h3>
        <form>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" placeholder="nombre" value="{!! $perrito->nombre !!}">
            </div>
            <div class="form-group">
                <label for="color">Color</label>
                <input type="text" class="form-control" id="color" placeholder="color" value="{!! $perrito->color !!}">
            </div>
            <div class="form-group">
                <label for="raza">Raza</label>
                <input type="text" class="form-control" id="raza" placeholder="raza" value="{!! $perrito->raza !!}">
            </div>
            <div class="col-auto">
                <button type="button" class="btn btn-primary" id="btn-agregar">Agregar</button>
            </div>
        </form>
    </div>
</div>