<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
        <title>Test perritos</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    </head>
    <body>
        <div class="container-fluid">
            <main id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <h3>Aplicación de perritos</h3><br>
                        <div class="col-sm-7 text-right">
                            <div class="btn-custom">
                                <button class="btn btn-primary btn-new" data-toggle="modal" data-target="#crear-perrito" href="perrito/create" title="Agregar un nuevo registro">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="perritos">
                        <div class="card">
                            <div class="card-header">Perritos registrados</div>
                            <div class="card-body">
                            <table class="table table-striped datatable table-hover display" data-ordering="true" id="tabla-perritos">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Color</th>
                                        <th scope="col">Raza</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                @isset($perritos)
                                <tbody>
                                    @foreach ($perritos as $key => $perrito)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $perrito->nombre }}</td>
                                        <td>{{ $perrito->color }}</td>
                                        <td>{{ $perrito->raza }}</td>
                                        <td>
                                            <a class="btn btn-secondary btn-sm btn-editar" data-toggle="modal" data-target="#modificar-perrito" 
                                            data-id="{{ $perrito->id }}"
                                            data-nombre="{{ $perrito->nombre }}"
                                            data-color="{{ $perrito->color }}"
                                            data-raza="{{ $perrito->raza }}"
                                            href="{{ url("perrito/" . $perrito->id . "/edit") }}" title="Modificar">
                                                <i class="fas fa-cog"></i>
                                            </a>
                                            <a class="btn btn-danger btn-sm btn-eliminar" data-toggle="modal" data-target="#modal-eliminar" data-id="{{ $perrito->id }}" title="Eliminar" id="">
                                                <i class="fas fa-trash" style="color:white"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @endisset
                            </table>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="modal fade" id="crear-perrito">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Agregar Perrito</h4>
                        <button type="button" class="close" data-dismiss="modal" >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <form>
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" placeholder="nombre">
                                </div>
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color" placeholder="color">
                                </div>
                                <div class="form-group">
                                    <label for="raza">Raza</label>
                                    <input type="text" class="form-control" id="raza" placeholder="raza">
                                </div>
                                <div>
                                    <button type="button" class="btn btn-primary" id="btn-agregar">Agregar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modificar-perrito">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar Perrito</h4>
                        <button type="button" class="close" data-dismiss="modal" >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <form>
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" id="edit-nombre" placeholder="nombre" value="">
                                </div>
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="edit-color" placeholder="color" value="">
                                </div>
                                <div class="form-group">
                                    <label for="raza">Raza</label>
                                    <input type="text" class="form-control" id="edit-raza" placeholder="raza" value="">
                                    <input type="hidden" id="edit-idperrito" value="0">
                                </div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-primary" id="btn-editar">Agregar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mensaje-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <p class="alert">Perrito agregado exitosamente</p>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ¿Seguro que desea eliminar al perrito?
                        <input type="hidden" id="delete-idperrito" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="confirmar">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </body>
</html>