<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditarPerritoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => ['required'],
            'color' => ['required'],
            'raza' => ['required']
        ];
    }

    public function messages()
    {
        $messages = [
            'nombre.required' => 'El nombre del perrito es obligatorio',
            'color.required' => 'El color del perrito es obligatorio',
            'raza.required' => 'La raza del perrito es obligatoria'
        ];
        return $messages;
    }
}
