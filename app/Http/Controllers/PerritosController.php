<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditarPerritoRequest;
use App\Http\Requests\IngresarPerritoRequest;
use Illuminate\Http\Request;
use App\Models\Perrito;

class PerritosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perritos = Perrito::all();
        return view('principal', compact('perritos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perritos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngresarPerritoRequest $request)
    {
        $perrito = new Perrito();
        $perrito->nombre = $request->nombre;
        $perrito->color = $request->color;
        $perrito->raza = $request->raza;
        if ($perrito->save()) {
            return response("Agregado exitosamente");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perrito = Perrito::find($id);
        return view('perritos.edit', compact('perrito'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditarPerritoRequest $request, $id)
    {
        $perrito = Perrito::find($id);
        $perrito->nombre = $request->nombre;
        $perrito->color = $request->color;
        $perrito->raza = $request->raza;
        if ($perrito->save()) {
            return response("Guardado con éxito");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $perrito = Perrito::find($id);
        if ($perrito->delete()) {
            return response("Eliminado con éxito");
        }
    }

    /**
     * Cree este método porque el show está destinado a mostrar perritos individualmente
     */

    public function mandarPerritosAjax()
    {
        $perritos = Perrito::all();
        return response($perritos);
    }
}
