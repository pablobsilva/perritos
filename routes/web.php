<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PerritosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PerritosController::class, 'index']);
Route::resource('perrito', PerritosController::class);
Route::get('perritos', [PerritosController::class, 'mandarPerritosAjax']);
